/*opam-version: "2.0"
  name: "lwt_log"
  version: "1.1.0"
  synopsis: "Lwt logging library (deprecated)"
  maintainer: "Anton Bachin <antonbachin@yahoo.com>"
  authors: ["Shawn Wagner" "Jérémie Dimino"]
  license: "LGPL"
  homepage: "https://github.com/aantron/lwt_log"
  doc:
   
  "https://github.com/aantron/lwt_log/blob/master/src/core/lwt_log_core.mli"
  bug-reports: "https://github.com/aantron/lwt_log/issues"
  depends: [
    "ocaml"
    "jbuilder" {build & >= "1.0+beta10"}
    "lwt" {>= "4.0.0"}
  ]
  build: ["jbuilder" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/aantron/lwt_log.git"
  url {
    src: "https://github.com/aantron/lwt_log/archive/1.1.0.tar.gz"
    checksum: [
      "md5=92142135d01a4d7e805990cc98653d55"
     
  "sha256=c2461379d71a22ba7cb8ee4c05ab3ed19d2fe876fad74e4ad4eb1a23841426d3"
     
  "sha512=fa0ce2928912b55ecbddc14ecd6f6d5db399f7cd6cba5f38d61db4c89ab7345d3e42f8f1292d9a5be973e16fd43d8381d6c269ae47f3692bb64ff567a7039451"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, jbuilder, lwt, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.0"; in
assert (vcompare jbuilder "1.0+beta10") >= 0;
assert (vcompare lwt "4.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "lwt_log";
  version = "1.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/aantron/lwt_log/archive/1.1.0.tar.gz";
    sha256 = "1lr62j2266pbsi54xmzsfvl2z7fi7smhak7fp1ybl8hssxwi6in2";
  };
  buildInputs = [
    ocaml jbuilder lwt findlib ];
  propagatedBuildInputs = [
    ocaml jbuilder lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'jbuilder'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
