/*opam-version: "2.0"
  name: "ezjsonm"
  version: "1.1.0"
  synopsis: "Simple interface on top of the Jsonm JSON library"
  description: """
  Ezjsonm provides more convenient (but far less flexible)
  input and output functions that go to and from `string` values.
  This avoids the need to write signal code, which is useful for
  quick scripts that manipulate JSON.
  
  More advanced users should go straight to the Jsonm library and
  use it directly, rather than be saddled with the Ezjsonm
  interface."""
  maintainer: "thomas@gazagnaire.org"
  authors: "Thomas Gazagnaire"
  license: "ISC"
  tags: ["org:mirage" "org:ocamllabs"]
  homepage: "https://github.com/mirage/ezjsonm"
  doc: "https://mirage.github.io/ezjsonm/"
  bug-reports: "https://github.com/mirage/ezjsonm/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build & >= "1.0"}
    "alcotest" {with-test & >= "0.4.0"}
    "jsonm" {>= "1.0.0"}
    "sexplib"
    "hex"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ezjsonm.git"
  url {
    src:
     
  "https://github.com/mirage/ezjsonm/releases/download/v1.1.0/ezjsonm-v1.1.0.tbz"
    checksum: [
      "md5=e8f207c6cd2226b2c4784b1e56556797"
     
  "sha256=527dbd3f930570ced1052f20b8844fe92a87adca7ec870fe9353695902f3d2b5"
     
  "sha512=25ba2d72fda09d42fea1020db1c301b50405a91019ee6f00073c31a2edfa9945e0d3b10bf32e817601b8ec19c56a64df38499f6803bf45135d3f4e3d5603c270"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, alcotest ? null, jsonm, sexplib, hex, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.0") >= 0;
assert doCheck -> (vcompare alcotest "0.4.0") >= 0;
assert (vcompare jsonm "1.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ezjsonm";
  version = "1.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ezjsonm/releases/download/v1.1.0/ezjsonm-v1.1.0.tbz";
    sha256 = "1dfjyc15jsakjgz71j3yranqfap99y2bh81g0p8www05jczvszaj";
  };
  buildInputs = [
    ocaml dune alcotest jsonm sexplib hex findlib ];
  propagatedBuildInputs = [
    ocaml dune alcotest jsonm sexplib hex ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
