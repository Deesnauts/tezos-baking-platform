/*opam-version: "2.0"
  name: "cohttp-lwt-unix"
  version: "2.0.0"
  synopsis: "CoHTTP implementation for Unix and Windows using
  Lwt"
  description: """
  An implementation of an HTTP client and server using the Lwt
  concurrency library. See the `Cohttp_lwt_unix` module for information
  on how to use this.  The package also installs `cohttp-curl-lwt`
  and a `cohttp-server-lwt` binaries for quick uses of a HTTP(S)
  client and server respectively.
  
  Although the name implies that this only works under Unix, it
  should also be fine under Windows too."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Stefano Zacchiroli"
    "David Sheets"
    "Thomas Gazagnaire"
    "David Scott"
    "Rudi Grinberg"
    "Andy Ray"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-cohttp"
  doc: "https://mirage.github.io/ocaml-cohttp/"
  bug-reports: "https://github.com/mirage/ocaml-cohttp/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {build & >= "1.1.0"}
    "conduit-lwt-unix" {>= "1.0.3"}
    "cmdliner"
    "magic-mime"
    "logs"
    "fmt" {>= "0.8.2"}
    "cohttp-lwt"
    "lwt" {>= "3.0.0"}
    "base-unix"
    "ounit" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cohttp.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cohttp/releases/download/v2.0.0/cohttp-v2.0.0.tbz"
    checksum: [
      "md5=c354599fdb4f2625b6510182de0fc86b"
     
  "sha256=1f6914190100b86c713b67fb5d9c7121aa477600255deac05cd5a9a6530dc7cb"
     
  "sha512=6100392e9f399140864c99195741e5e46ce454d973a693bb2161f334592a55256253dd0912384480ce0da545e05bb24c13de894862b4e2081a48cf9231aa46ee"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, conduit-lwt-unix, cmdliner, magic-mime, logs, fmt,
  cohttp-lwt, lwt, base-unix, ounit ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.0.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.1.0") >= 0;
assert (vcompare conduit-lwt-unix "1.0.3") >= 0;
assert (vcompare fmt "0.8.2") >= 0;
assert (vcompare lwt "3.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "cohttp-lwt-unix";
  version = "2.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cohttp/releases/download/v2.0.0/cohttp-v2.0.0.tbz";
    sha256 = "1jy71m9sdafmbk0flp9501v4gai1f6f5vyv77dqnrf0004ci8s8z";
  };
  buildInputs = [
    ocaml dune conduit-lwt-unix cmdliner magic-mime logs fmt cohttp-lwt lwt
    base-unix ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml dune conduit-lwt-unix cmdliner magic-mime logs fmt cohttp-lwt lwt
    base-unix ]
  ++
  stdenv.lib.optional
  doCheck
  ounit;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
