/*opam-version: "2.0"
  name: "genspio"
  version: "0.0.2"
  synopsis: "Typed EDSL to generate POSIX Shell scripts"
  description: """
  Genspio is a typed EDSL used to generate shell scripts and commands from
  OCaml.
  
  The idea is to build values of type `'a Genspio.EDSL.t` with the
  combinators in the `Genspio.EDSL` module, and compile them to POSIX
  shell scripts (or one-liners) with functions from
  `Genspio.Compile`.
  
  Genspio's documentation root is at
  <https://smondet.gitlab.io/genspio-doc/>."""
  maintainer: "Seb Mondet <seb@mondet.org>"
  authors: "Seb Mondet <seb@mondet.org>"
  license: "Apache 2.0"
  homepage: "https://smondet.gitlab.io/genspio-doc/"
  bug-reports: "https://github.com/hammerlab/genspio/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "jbuilder" {build & >= "1.0+beta20"}
    "nonstd"
    "sosa"
  ]
  build: [
    ["ocaml" "please.mlt" "configure"]
    ["jbuilder" "build" "-p" "genspio" "-j" jobs]
  ]
  dev-repo: "git+https://github.com/hammerlab/genspio.git"
  url {
    src: "https://github.com/hammerlab/genspio/archive/genspio.0.0.2.tar.gz"
    checksum: [
      "md5=03a6e537a18d4f9b23301beb53ddd0be"
     
  "sha256=a3725f0bde964f496907ddb17e9806f64ec15477bddc664b33166c54eb7580de"
     
  "sha512=bd2bb03320cf21b911d0d626e4124626887dca2450d2b5105ec4e2f2240e763c84a68042ec6fcd028162660d3a97c65fa06e4eb3002cfc0a5740e94c17aecc29"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, jbuilder, nonstd, sosa, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.0.2"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare jbuilder "1.0+beta20") >= 0;

stdenv.mkDerivation rec {
  pname = "genspio";
  version = "0.0.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/hammerlab/genspio/archive/genspio.0.0.2.tar.gz";
    sha256 = "1pl0fpmm8v0n6d5ndp5xfxac2kpn0sc7xcfx0xlljkwnvq5mywm3";
  };
  buildInputs = [
    ocaml jbuilder nonstd sosa findlib ];
  propagatedBuildInputs = [
    ocaml jbuilder nonstd sosa ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'ocaml'" "'please.mlt'" "'configure'" ] [
      "'jbuilder'" "'build'" "'-p'" "'genspio'" "'-j'" "1" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
