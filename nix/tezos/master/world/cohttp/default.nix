/*opam-version: "2.0"
  name: "cohttp"
  version: "2.5.0"
  synopsis: "An OCaml library for HTTP clients and servers"
  description: """
  Cohttp is an OCaml library for creating HTTP daemons. It has a portable
  HTTP parser, and implementations using various asynchronous
  programming
  libraries.
  
  See the cohttp-async, cohttp-lwt, cohttp-lwt-unix, cohttp-lwt-jsoo
  and
  cohttp-mirage libraries for concrete implementations for
  particular
  targets.
  
  You can implement other targets using the parser very easily. Look at the
  `IO`
  signature in `lib/s.mli` and implement that in the desired backend.
  
  You can activate some runtime debugging by setting `COHTTP_DEBUG` to
  any
  value, and all requests and responses will be written to stderr. 
  Further
  debugging of the connection layer can be obtained by setting
  `CONDUIT_DEBUG`
  to any value."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Stefano Zacchiroli"
    "David Sheets"
    "Thomas Gazagnaire"
    "David Scott"
    "Rudi Grinberg"
    "Andy Ray"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-cohttp"
  doc: "https://mirage.github.io/ocaml-cohttp/"
  bug-reports: "https://github.com/mirage/ocaml-cohttp/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {>= "1.1.0"}
    "re" {>= "1.9.0"}
    "uri" {>= "2.0.0"}
    "uri-sexp"
    "fieldslib"
    "sexplib0"
    "ppx_fields_conv" {>= "v0.9.0"}
    "ppx_sexp_conv" {>= "v0.13.0"}
    "stringext"
    "base64" {>= "3.1.0"}
    "stdlib-shims"
    "fmt" {with-test}
    "jsonm" {build}
    "alcotest" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cohttp.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cohttp/releases/download/v2.5.0/cohttp-v2.5.0.tbz"
    checksum: [
     
  "sha256=f07905bbe3138425572406844585e83ecb19ba94a8932b8e12d705cc32eada5a"
     
  "sha512=02af7b18cea62241bae3dd6112a4fc0152c978b87358cf03fa4338ff9dadfcf6b86facbcdd7dd71c5dba5b6e98232099b80b2803d4a282feafa998cd895f3ce8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, re, uri, uri-sexp, fieldslib, sexplib0,
  ppx_fields_conv, ppx_sexp_conv, stringext, base64, stdlib-shims,
  fmt ? null, jsonm, alcotest ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.5.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.1.0") >= 0;
assert (vcompare re "1.9.0") >= 0;
assert (vcompare uri "2.0.0") >= 0;
assert (vcompare ppx_fields_conv "v0.9.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.13.0") >= 0;
assert (vcompare base64 "3.1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "cohttp";
  version = "2.5.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cohttp/releases/download/v2.5.0/cohttp-v2.5.0.tbz";
    sha256 = "0nnsx8rcq1fp2a72p4x8jjx1kjryx22lb1064ibjb10kwfxhaygh";
  };
  buildInputs = [
    ocaml dune re uri uri-sexp fieldslib sexplib0 ppx_fields_conv
    ppx_sexp_conv stringext base64 stdlib-shims ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  [
    jsonm ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml dune re uri uri-sexp fieldslib sexplib0 ppx_fields_conv
    ppx_sexp_conv stringext base64 stdlib-shims ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
