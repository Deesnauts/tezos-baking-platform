/*opam-version: "2.0"
  name: "afl-persistent"
  version: "1.3"
  synopsis: "Use afl-fuzz in persistent mode"
  description: """
  afl-fuzz normally works by repeatedly fork()ing the program being
  tested. using this package, you can run afl-fuzz in 'persistent mode',
  which avoids repeated forking and is much faster."""
  maintainer: "stephen.dolan@cl.cam.ac.uk"
  authors: "Stephen Dolan"
  license: "MIT"
  homepage: "https://github.com/stedolan/ocaml-afl-persistent"
  bug-reports:
  "https://github.com/stedolan/ocaml-afl-persistent/issues"
  depends: [
    "ocaml" {>= "4.00"}
    "base-unix"
  ]
  build: "./build.sh"
  post-messages: [
    """
  afl-persistent is installed, but since AFL instrumentation is not
  available
  with this OCaml compiler, instrumented fuzzing with afl-fuzz won't work.
  
  To use instrumented fuzzing, switch to an OCaml version supporting AFL,
  such
  as 4.07.1+afl."""
      {success & !afl-available}
    """
  afl-persistent is installed, but since the current OCaml compiler does
  not enable AFL instrumentation by default, most packages will not
  be
  instrumented and fuzzing with afl-fuzz may not be effective.
  
  To globally enable AFL instrumentation, use an OCaml switch such
  as
  4.07.1+afl."""
      {success & afl-available & !afl-always}
  ]
  dev-repo: "git+https://github.com/stedolan/ocaml-afl-persistent.git"
  url {
    src:
  "https://github.com/stedolan/ocaml-afl-persistent/archive/v1.3.tar.gz"
    checksum: [
      "md5=613ae369149ae8e2942e3c8bddd6798b"
     
  "sha256=1148c43b9548a7ee146b9951dd2700bb303eeb11c76222077346620153a50e5d"
     
  "sha512=31f940be1c3e79856a001e2af687bb60f1a1bd6e04c6e730dc8f859db22bfccdc38eddb50baef423473cae9d1d3c2b86427a8257df0005328027b0fa2735100a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base-unix, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.3"; in
assert (vcompare ocaml "4.00") >= 0;

stdenv.mkDerivation rec {
  pname = "afl-persistent";
  version = "1.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/stedolan/ocaml-afl-persistent/archive/v1.3.tar.gz";
    sha256 = "0p8flm9h2qj6fc3j4qn727mkwc5v00kxslcrdcafx9s8jlxw8j0i";
  };
  buildInputs = [
    ocaml base-unix findlib ];
  propagatedBuildInputs = [
    ocaml base-unix ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'./build.sh'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
