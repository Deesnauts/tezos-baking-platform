/*opam-version: "2.0"
  name: "stdio"
  version: "v0.13.0"
  synopsis: "Standard IO library for OCaml"
  description: """
  Stdio implements simple input/output functionalities for OCaml.
  
  It re-exports the input/output functions of the OCaml standard
  libraries using a more consistent API."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/stdio"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/stdio/index.html"
  bug-reports: "https://github.com/janestreet/stdio/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/stdio.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/stdio-v0.13.0.tar.gz"
    checksum: [
      "md5=48ef28512ddd51ff9885649dd1fab91d"
     
  "sha256=95efc7a8f422a8d367309ed2d73cd68dfd8181e7d61ca80a6cce1e01d604662d"
     
  "sha512=cc10fa7ecd14d8c164141772cee85e09250edf90c346b01482e4a9bb6bfb34bd174c0cc40feb59aa27372899048e2b5b31c0ad1bd634a6ed8b9c8b8e2c48d7a5"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.13") >= 0 && (vcompare base "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "stdio";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/stdio-v0.13.0.tar.gz";
    sha256 = "0bb60kb027nfdh5ah76nwy0q3zcdsqydglly61kx7a12yjlcgvwm";
  };
  buildInputs = [
    ocaml base dune findlib ];
  propagatedBuildInputs = [
    ocaml base dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
