/*opam-version: "2.0"
  name: "lwt-canceler"
  version: "0.2"
  synopsis: "Cancellation synchronization object"
  maintainer: "contact@nomadic-labs.com"
  authors: "Nomadic Labs"
  license: "MIT"
  homepage: "https://gitlab.com/nomadic-labs/lwt-canceler"
  bug-reports: "https://gitlab.com/nomadic-labs/lwt-canceler/issues"
  depends: [
    "ocaml"
    "dune" {>= "1.7"}
    "lwt" {>= "3.0.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://gitlab.com/nomadic-labs/lwt-canceler.git"
  url {
    src:
     
  "https://gitlab.com/nomadic-labs/lwt-canceler/-/archive/v0.2/lwt-canceler-v0.2.tar.gz"
    checksum: [
      "md5=c0e1d94b178e8c2bb38bc1086c11a029"
     
  "sha512=b3c23f82ee293cc4299be1cf41b94ac6592ce8d3124cb8ef1380ac258bd3c8ca5635387943b6ce03048c163d76d0157097c26a7c6dd2139c78dde27d1c622296"
     
  "sha256=aeed3ceb6a930bda03b4cac9c87a6384f6f98017a20446d267aa9adb2de587d8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, lwt, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.2"; in
assert (vcompare dune "1.7") >= 0;
assert (vcompare lwt "3.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "lwt-canceler";
  version = "0.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://gitlab.com/nomadic-labs/lwt-canceler/-/archive/v0.2/lwt-canceler-v0.2.tar.gz";
    sha256 = "1n47wlnxp6macz94c1522y0gkxl4cdxcijfanh1xl2wkdbmkrvdf";
  };
  buildInputs = [
    ocaml dune lwt findlib ];
  propagatedBuildInputs = [
    ocaml dune lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
