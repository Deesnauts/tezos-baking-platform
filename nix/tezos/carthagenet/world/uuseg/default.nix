/*opam-version: "2.0"
  name: "uuseg"
  version: "12.0.0"
  synopsis: "Unicode text segmentation for OCaml"
  description: """
  Uuseg is an OCaml library for segmenting Unicode text. It implements
  the locale independent [Unicode text segmentation algorithms][1] to
  detect grapheme cluster, word and sentence boundaries and the
  [Unicode line breaking algorithm][2] to detect line break
  opportunities.
  
  The library is independent from any IO mechanism or Unicode text
  data
  structure and it can process text without a complete
  in-memory
  representation.
  
  Uuseg depends on [Uucp](http://erratique.ch/software/uucp) and
  optionally on [Uutf](http://erratique.ch/software/uutf) for support on
  OCaml UTF-X encoded strings. It is distributed under the ISC license.
  
  [1]: http://www.unicode.org/reports/tr29/
  [2]: http://www.unicode.org/reports/tr14/"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  license: "ISC"
  tags: ["segmentation" "text" "unicode" "org:erratique"]
  homepage: "https://erratique.ch/software/uuseg"
  doc: "https://erratique.ch/software/uuseg"
  bug-reports: "https://github.com/dbuenzli/uuseg/issues"
  depends: [
    "ocaml" {>= "4.01.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "uchar"
    "uucp" {>= "12.0.0" & < "13.0.0"}
  ]
  depopts: [
    "uutf"
    "cmdliner"
    "uutf" {with-test}
    "cmdliner" {with-test}
  ]
  conflicts: [
    "uutf" {< "1.0.0"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--pinned"
    "%{pinned}%"
    "--with-uutf"
    "%{uutf:installed}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/uuseg.git"
  url {
    src: "https://erratique.ch/software/uuseg/releases/uuseg-12.0.0.tbz"
    checksum: [
      "md5=1d4487ddf5154e3477e55021b978d58a"
     
  "sha256=3934a317e7edde2a5b4489c86890f5a9f79feecc9a33eb7d19ee01c2f9939321"
     
  "sha512=e3559bb3309ade64c47bbfbc12dbe5b9d26cd400945606789fcf144ac986ac5397fd951b5f53d4c915ee2127ebcbe6aa66342f7d29e531b077d1f4992be369b3"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, uchar, uucp, uutf ? null,
  cmdliner ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "12.0.0"; in
assert (vcompare ocaml "4.01.0") >= 0;
assert (vcompare uucp "12.0.0") >= 0 && (vcompare uucp "13.0.0") < 0;
assert uutf != null -> !((vcompare uutf "1.0.0") < 0);

stdenv.mkDerivation rec {
  pname = "uuseg";
  version = "12.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/uuseg/releases/uuseg-12.0.0.tbz";
    sha256 = "08ckjgww40gf35yyncwsrkp9zxx9yn86ij498idjmppdwwbs6d1r";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg uchar uucp ]
  ++
  stdenv.lib.optional
  (uutf
  !=
  null
  ||
  uutf
  !=
  null)
  uutf
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null
  ||
  cmdliner
  !=
  null)
  cmdliner;
  propagatedBuildInputs = [
    ocaml uchar uucp ]
  ++
  stdenv.lib.optional
  (uutf
  !=
  null
  ||
  uutf
  !=
  null)
  uutf
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null
  ||
  cmdliner
  !=
  null)
  cmdliner;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false" "'--with-uutf'"
      "${if uutf != null then "true" else "false"}" "'--with-cmdliner'" "${if
                                                                    cmdliner
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] ];
      preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
      [ ];
      installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
      createFindlibDestdir = true;
    }
