/*opam-version: "2.0"
  name: "result"
  version: "1.4"
  synopsis: "Compatibility Result module"
  description: """
  Projects that want to use the new result type defined in OCaml >= 4.03
  while staying compatible with older version of OCaml should use the
  Result module defined in this library."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "BSD-3-Clause"
  homepage: "https://github.com/janestreet/result"
  bug-reports: "https://github.com/janestreet/result/issues"
  depends: [
    "ocaml"
    "dune" {>= "1.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/result.git"
  url {
    src: "https://github.com/janestreet/result/archive/1.4.tar.gz"
    checksum: [
      "md5=d3162dbc501a2af65c8c71e0866541da"
     
  "sha256=167029f0d0475106200697f3dffda20b2462a345fe35b449fe86f1f92db354b2"
     
  "sha512=2e709fee6ceb54463c3989a90aed351c5b48f7a5edce9ccbba4d163cbfb795a19393be0d75885e762d4609961a64e273bb298b94bd3858dc2c20de9396b655d3"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.4"; in
assert (vcompare dune "1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "result";
  version = "1.4";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/janestreet/result/archive/1.4.tar.gz";
    sha256 = "1cjlncnzkwc6zr4v8dgy8nin490blbyxzwwp0qh0cla7s3q2jw0n";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
