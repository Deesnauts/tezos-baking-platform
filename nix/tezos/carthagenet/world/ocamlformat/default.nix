/*opam-version: "2.0"
  name: "ocamlformat"
  version: "0.10"
  synopsis: "Auto-formatter for OCaml code"
  description:
    "OCamlFormat is a tool to automatically format OCaml code in a uniform
  style."
  maintainer: "OCamlFormat Team <ocamlformat-team@fb.com>"
  authors: "Josh Berdine <jjb@fb.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml-ppx/ocamlformat"
  bug-reports: "https://github.com/ocaml-ppx/ocamlformat/issues"
  depends: [
    "ocaml" {>= "4.06"}
    "base" {>= "v0.11.0" & < "v0.13"}
    "base-unix"
    "cmdliner"
    "dune" {>= "1.1.1"}
    "fpath"
    "ocaml-migrate-parsetree" {>= "1.3.1"}
    "octavius" {>= "1.2.0"}
    "stdio" {< "v0.13"}
    "uuseg" {>= "10.0.0"}
    "uutf" {>= "1.0.1"}
  ]
  build: [
    ["ocaml" "tools/gen_version.mlt" "src/Version.ml" version] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/ocaml-ppx/ocamlformat.git"
  url {
    src: "https://github.com/ocaml-ppx/ocamlformat/archive/0.10.tar.gz"
    checksum: [
      "md5=7f3531e0cd326c7f33d365eb99916a7b"
     
  "sha512=6577a13658a9f6f33cb6470d31c6366feea6efb54ddaf2a3407fdb6d0878a369409e50092ef26bf65fd52ddd715ae658c33954e21d1cb3fd4d580e04fe8b0f86"
     
  "sha256=77165f4ef44578fc31e135b7971afbb829a15c43227741df5767219a42509d29"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, base-unix, cmdliner, dune, fpath,
  ocaml-migrate-parsetree, octavius, stdio, uuseg, uutf, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.10"; in
assert (vcompare ocaml "4.06") >= 0;
assert (vcompare base "v0.11.0") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare dune "1.1.1") >= 0;
assert (vcompare ocaml-migrate-parsetree "1.3.1") >= 0;
assert (vcompare octavius "1.2.0") >= 0;
assert (vcompare stdio "v0.13") < 0;
assert (vcompare uuseg "10.0.0") >= 0;
assert (vcompare uutf "1.0.1") >= 0;

stdenv.mkDerivation rec {
  pname = "ocamlformat";
  version = "0.10";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ocamlformat/archive/0.10.tar.gz";
    sha256 = "0acxa119l8b7azgl2xr28dfa2adqzcd9gdrmw4qzqy25yi75y5kp";
  };
  buildInputs = [
    ocaml base base-unix cmdliner dune fpath ocaml-migrate-parsetree octavius
    stdio uuseg uutf findlib ];
  propagatedBuildInputs = [
    ocaml base base-unix cmdliner dune fpath ocaml-migrate-parsetree octavius
    stdio uuseg uutf ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
