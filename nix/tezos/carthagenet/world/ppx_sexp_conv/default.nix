/*opam-version: "2.0"
  name: "ppx_sexp_conv"
  version: "v0.12.0"
  synopsis: "[@@deriving] plugin to generate S-expression conversion
  functions"
  description: "Part of the Jane Street's PPX rewriters
  collection."
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/ppx_sexp_conv"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_sexp_conv/index.html"
  bug-reports: "https://github.com/janestreet/ppx_sexp_conv/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.12" & < "v0.13"}
    "sexplib0" {>= "v0.12" & < "v0.13"}
    "dune" {>= "1.5.1"}
    "ppxlib" {>= "0.5.0" & < "0.9.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ppx_sexp_conv.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/ppx_sexp_conv-v0.12.0.tar.gz"
    checksum: [
      "md5=648ac430b4a74c2297705d260b66778f"
     
  "sha256=6b744865f0226172a330c9275e425ad3ea7be5b74f96dea9884d360ce5ef0025"
     
  "sha512=0ea9073362e5987254d150c7598bf70426915e0f4568268cf072990337c49937583c132e2169636e5da8cccce46426929e8d9cb9d53e4ff6e974290189fb22c0"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, sexplib0, dune, ppxlib, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.12") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare sexplib0 "v0.12") >= 0 && (vcompare sexplib0 "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;
assert (vcompare ppxlib "0.5.0") >= 0 && (vcompare ppxlib "0.9.0") < 0;

stdenv.mkDerivation rec {
  pname = "ppx_sexp_conv";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/ppx_sexp_conv-v0.12.0.tar.gz";
    sha256 = "0980xzjhqdjdi2lxx5jgnzjppsnkb915w9y962ip4q92y1jlhx3b";
  };
  buildInputs = [
    ocaml base sexplib0 dune ppxlib findlib ];
  propagatedBuildInputs = [
    ocaml base sexplib0 dune ppxlib ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
