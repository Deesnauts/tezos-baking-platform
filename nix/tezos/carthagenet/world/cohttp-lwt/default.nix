/*opam-version: "2.0"
  name: "cohttp-lwt"
  version: "2.4.0"
  synopsis: "CoHTTP implementation using the Lwt concurrency
  library"
  description: """
  This is a portable implementation of HTTP that uses the Lwt
  concurrency library to multiplex IO.  It implements as much of the
  logic in an OS-independent way as possible, so that more specialised
  modules can be tailored for different targets.  For example, you
  can install `cohttp-lwt-unix` or `cohttp-lwt-jsoo` for a Unix or
  JavaScript backend, or `cohttp-mirage` for the MirageOS unikernel
  version of the library. All of these implementations share the same
  IO logic from this module."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Stefano Zacchiroli"
    "David Sheets"
    "Thomas Gazagnaire"
    "David Scott"
    "Rudi Grinberg"
    "Andy Ray"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-cohttp"
  doc: "https://mirage.github.io/ocaml-cohttp/"
  bug-reports: "https://github.com/mirage/ocaml-cohttp/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {>= "1.1.0"}
    "cohttp" {= version}
    "lwt" {>= "2.5.0"}
    "sexplib0" {< "v0.13"}
    "ppx_sexp_conv" {>= "v0.9.0" & < "v0.13"}
    "logs"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cohttp.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cohttp/releases/download/v2.4.0/cohttp-v2.4.0.tbz"
    checksum: [
     
  "sha256=b2dc17836e7dd5803bb919aff5dde1b9a071becb874c8a4afc81b2a466c792f4"
     
  "sha512=73a5ef380ea6aad421870b526e5e1305ad0a0d14cee7ca7fdbf5a94f878c66bc91a9f7c30327caaf235f3e55c678299ad55645ef2899e11b137efb6ecfc5d0be"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cohttp, lwt, sexplib0, ppx_sexp_conv, logs, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.4.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.1.0") >= 0;
assert stdenv.lib.getVersion cohttp == version;
assert (vcompare lwt "2.5.0") >= 0;
assert (vcompare sexplib0 "v0.13") < 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0 && (vcompare ppx_sexp_conv
  "v0.13") < 0;

stdenv.mkDerivation rec {
  pname = "cohttp-lwt";
  version = "2.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cohttp/releases/download/v2.4.0/cohttp-v2.4.0.tbz";
    sha256 = "1x4jqxka9cl1zi58lk47rfz7385rw7fzbbqrp4xq1mbxds1igp5j";
  };
  buildInputs = [
    ocaml dune cohttp lwt sexplib0 ppx_sexp_conv logs findlib ];
  propagatedBuildInputs = [
    ocaml dune cohttp lwt sexplib0 ppx_sexp_conv logs ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
