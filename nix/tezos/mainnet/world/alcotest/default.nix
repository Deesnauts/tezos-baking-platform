/*opam-version: "2.0"
  name: "alcotest"
  version: "0.8.5"
  synopsis: "Alcotest is a lightweight and colourful test
  framework"
  description: """
  Alcotest exposes simple interface to perform unit tests. It exposes
  a simple TESTABLE module type, a check function to assert test
  predicates and a run function to perform a list of unit -> unit
  test callbacks.
  
  Alcotest provides a quiet and colorful output where only faulty runs
  are fully displayed at the end of the run (with the full logs ready
  to
  inspect), with a simple (yet expressive) query language to select the
  tests to run."""
  maintainer: "thomas@gazagnaire.org"
  authors: "Thomas Gazagnaire"
  license: "ISC"
  homepage: "https://github.com/mirage/alcotest/"
  doc: "https://mirage.github.io/alcotest/"
  bug-reports: "https://github.com/mirage/alcotest/issues/"
  depends: [
    "dune" {>= "1.1.0"}
    "ocaml" {>= "4.02.3"}
    "fmt" {>= "0.8.0"}
    "astring"
    "result"
    "cmdliner"
    "uuidm"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/alcotest.git"
  url {
    src:
     
  "https://github.com/mirage/alcotest/releases/download/0.8.5/alcotest-0.8.5.tbz"
    checksum: [
      "md5=2db36741c413ab93391ecc1f983aa804"
     
  "sha256=6b3b638fc7c6f4c52617b0261dc9f726ce21bb0c485e05bab6fe41a57697fc6b"
     
  "sha512=2d4aaec0a382fb4a883b3dc127e5a62272ecc721f6a1cc0bfe3aebba8bf76efc4bce2e097b2043a8b4b618323162f18d34b610ea8c12787c15210724a7523fa3"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, dune, ocaml, fmt, astring, ocaml-result, cmdliner, uuidm, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.5"; in
assert (vcompare dune "1.1.0") >= 0;
assert (vcompare ocaml "4.02.3") >= 0;
assert (vcompare fmt "0.8.0") >= 0;

stdenv.mkDerivation rec {
  pname = "alcotest";
  version = "0.8.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/alcotest/releases/download/0.8.5/alcotest-0.8.5.tbz";
    sha256 = "0szwjxvaahgynsx0apj81jxj3ki6yz4is9mh2wkcbx66qy7n6fvb";
  };
  buildInputs = [
    dune ocaml fmt astring ocaml-result cmdliner uuidm findlib ];
  propagatedBuildInputs = [
    dune ocaml fmt astring ocaml-result cmdliner uuidm ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
