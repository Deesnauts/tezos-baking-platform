/*opam-version: "2.0"
  name: "conduit-lwt"
  version: "2.0.2"
  synopsis: "A portable network connection establishment library using
  Lwt"
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy" "Thomas Leonard" "Thomas Gazagnaire" "Rudi
  Grinberg"
  ]
  license: "ISC"
  tags: "org:mirage"
  homepage: "https://github.com/mirage/ocaml-conduit"
  bug-reports: "https://github.com/mirage/ocaml-conduit/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune"
    "base-unix"
    "ppx_sexp_conv" {>= "v0.9.0" & < "v0.13"}
    "sexplib" {< "v0.13"}
    "conduit" {= version}
    "lwt" {>= "3.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-conduit.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-conduit/releases/download/v2.0.2/conduit-v2.0.2.tbz"
    checksum: [
     
  "sha256=2510372ed98c7e0446d788317a435f752d900d72df0fbe4c353f5e5bfb9d1dd2"
     
  "sha512=3e25b754c84dd603acbb4d810b532c3cfb273808b9bf9a17890e40b79e65529d17cd66d613a447cb2a7f51f0522f17d46ab0ade5c79cb2a3c8565efd484238ae"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, base-unix, ppx_sexp_conv, sexplib, conduit, lwt,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.0.2"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0 && (vcompare ppx_sexp_conv
  "v0.13") < 0;
assert (vcompare sexplib "v0.13") < 0;
assert stdenv.lib.getVersion conduit == version;
assert (vcompare lwt "3.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "conduit-lwt";
  version = "2.0.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-conduit/releases/download/v2.0.2/conduit-v2.0.2.tbz";
    sha256 = "1lhxkpxmnpiz6m6bw3yzf86r0bbmbx1plcc8sx308zlcv4p3f415";
  };
  buildInputs = [
    ocaml dune base-unix ppx_sexp_conv sexplib conduit lwt findlib ];
  propagatedBuildInputs = [
    ocaml dune base-unix ppx_sexp_conv sexplib conduit lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
