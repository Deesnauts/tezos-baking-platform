/*opam-version: "2.0"
  name: "ppx_deriving"
  version: "4.4"
  synopsis: "Type-driven code generation for OCaml >=4.02.2"
  description: """
  ppx_deriving provides common infrastructure for generating
  code based on type definitions, and a set of useful plugins
  for common tasks."""
  maintainer: "whitequark <whitequark@whitequark.org>"
  authors: "whitequark <whitequark@whitequark.org>"
  license: "MIT"
  tags: "syntax"
  homepage: "https://github.com/ocaml-ppx/ppx_deriving"
  doc: "https://ocaml-ppx.github.io/ppx_deriving/"
  bug-reports: "https://github.com/ocaml-ppx/ppx_deriving/issues"
  depends: [
    "dune" {>= "1.6.3"}
    "cppo" {build}
    "ppxfind" {build}
    "ocaml-migrate-parsetree"
    "ppx_derivers"
    "ppx_tools" {>= "4.02.3"}
    "result"
    "ounit" {with-test}
    "ocaml" {>= "4.02" & < "4.10.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs]
      {with-test & ocaml:version >= "4.03"}
    ["dune" "build" "@doc" "-p" name "-j" jobs] {with-doc}
  ]
  dev-repo: "git+https://github.com/ocaml-ppx/ppx_deriving.git"
  url {
    src: "https://github.com/ocaml-ppx/ppx_deriving/archive/v4.4.tar.gz"
    checksum: [
     
  "sha256=c2d85af4cb65a1f163f624590fb0395a164bbfd0d05082092526b669e66bcc34"
     
  "sha512=d347a9b1bdec6f0c581737074068ee811a045c8207f938382011ee8ffbbe5659ac878ca2aee15461de08984d2cc2aa8437fbccfeae364730780f6d7a5019469c"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, dune, cppo, ppxfind, ocaml-migrate-parsetree, ppx_derivers,
  ppx_tools, ocaml-result, ounit ? null, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "4.4"; in
assert (vcompare dune "1.6.3") >= 0;
assert (vcompare ppx_tools "4.02.3") >= 0;
assert (vcompare ocaml "4.02") >= 0 && (vcompare ocaml "4.10.0") < 0;

stdenv.mkDerivation rec {
  pname = "ppx_deriving";
  version = "4.4";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppx_deriving/archive/v4.4.tar.gz";
    sha256 = "0d6cdgk6kdi64l4q4l6hs2zln5js76q0yn94yriz38b5rgs5mn62";
  };
  buildInputs = [
    dune cppo ppxfind ocaml-migrate-parsetree ppx_derivers ppx_tools
    ocaml-result ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ocaml findlib ];
  propagatedBuildInputs = [
    dune ocaml-migrate-parsetree ppx_derivers ppx_tools ocaml-result ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    (doCheck && (vcompare (stdenv.lib.getVersion ocaml) "4.03") >= 0) [
      "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ])
    (stdenv.lib.optionals buildDocs [
      "'dune'" "'build'" "'@doc'" "'-p'" pname "'-j'" "1" ])
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
