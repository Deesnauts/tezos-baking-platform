/*opam-version: "2.0"
  name: "js_of_ocaml"
  version: "3.4.0"
  synopsis: "Compiler from OCaml bytecode to Javascript"
  description: """
  Js_of_ocaml is a compiler from OCaml bytecode to JavaScript.
  It makes it possible to run pure OCaml programs in JavaScript
  environment like browsers and Node.js"""
  maintainer: "dev@ocsigen.org"
  authors: "Ocsigen team"
  homepage: "http://ocsigen.org/js_of_ocaml"
  bug-reports: "https://github.com/ocsigen/js_of_ocaml/issues"
  depends: [
    "ocaml" {>= "4.02.0" & < "4.09.0"}
    "dune" {>= "1.2"}
    "ocaml-migrate-parsetree"
    "ppx_tools_versioned"
    "uchar"
    "js_of_ocaml-compiler"
  ]
  conflicts: [
    "ppx_tools_versioned" {<= "5.0beta0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocsigen/js_of_ocaml.git"
  url {
    src: "https://github.com/ocsigen/js_of_ocaml/archive/3.4.0.tar.gz"
    checksum: [
      "md5=42f914d0410787d65668d2822fdab1a1"
     
  "sha256=c4fad35dc113d5d710d93fdc81b798d98bc33e675b758587279cd65160178db0"
     
  "sha512=6ad8afee5836767431743cad5fc9f60dc3530790a659bdaedbfca90ace68155f161cbc6bf4a1fa55ed6b03ce83729f87452aa7ba762559345c3c56a28f810072"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ocaml-migrate-parsetree, ppx_tools_versioned, uchar,
  js_of_ocaml-compiler, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.4.0"; in
assert (vcompare ocaml "4.02.0") >= 0 && (vcompare ocaml "4.09.0") < 0;
assert (vcompare dune "1.2") >= 0;
assert !((vcompare ppx_tools_versioned "5.0beta0") <= 0);

stdenv.mkDerivation rec {
  pname = "js_of_ocaml";
  version = "3.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocsigen/js_of_ocaml/archive/3.4.0.tar.gz";
    sha256 = "1c4d2xh53mlw4y3qaxavcwzc72yrk2vq3p1zv48dgm8kq5fx7yn4";
  };
  buildInputs = [
    ocaml dune ocaml-migrate-parsetree ppx_tools_versioned uchar
    js_of_ocaml-compiler findlib ];
  propagatedBuildInputs = [
    ocaml dune ocaml-migrate-parsetree ppx_tools_versioned uchar
    js_of_ocaml-compiler ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
