/*opam-version: "2.0"
  name: "fmt"
  version: "0.8.6"
  synopsis: "OCaml Format pretty-printer combinators"
  description: """
  Fmt exposes combinators to devise `Format` pretty-printing functions.
  
  Fmt depends only on the OCaml standard library. The optional
  `Fmt_tty`
  library that allows to setup formatters for terminal color output
  depends on the Unix library. The optional `Fmt_cli` library that
  provides command line support for Fmt depends on [`Cmdliner`][cmdliner].
  
  Fmt is distributed under the ISC license.
  
  [cmdliner]: http://erratique.ch/software/cmdliner"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "The fmt programmers"
  license: "ISC"
  tags: ["string" "format" "pretty-print" "org:erratique"]
  homepage: "https://erratique.ch/software/fmt"
  doc: "https://erratique.ch/software/fmt"
  bug-reports: "https://github.com/dbuenzli/fmt/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build & >= "0.9.0"}
    "seq"
    "stdlib-shims"
  ]
  depopts: ["base-unix" "cmdliner"]
  conflicts: [
    "cmdliner" {< "0.9.8"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--dev-pkg"
    "%{pinned}%"
    "--with-base-unix"
    "%{base-unix:installed}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/fmt.git"
  url {
    src: "https://erratique.ch/software/fmt/releases/fmt-0.8.6.tbz"
    checksum: [
      "md5=5407789e5f0ea42272ca19353b1abfd3"
     
  "sha256=36f6a18e9b7d1fc4711e9ea9ca45911bc88106b8f9a0887f5381e3fd7e2c9cca"
     
  "sha512=993444fbc41609103dd92bc23bdaf5edb9526835d9029b8bf62546c4de74a8aadbc62c187670985210c4fc4711eab7042f2cd72d18130790bb137cabced4c791"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, seq, stdlib-shims,
  base-unix ? null, cmdliner ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.6"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare topkg "0.9.0") >= 0;
assert cmdliner != null -> !((vcompare cmdliner "0.9.8") < 0);

stdenv.mkDerivation rec {
  pname = "fmt";
  version = "0.8.6";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/fmt/releases/fmt-0.8.6.tbz";
    sha256 = "1jlw5izgvqw1adzqi87rp0383j0vj52wmacy3rqw87vxkf7a3xin";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg seq stdlib-shims ]
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  propagatedBuildInputs = [
    ocaml topkg seq stdlib-shims ]
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--dev-pkg'" "false"
      "'--with-base-unix'"
      "${if base-unix != null then "true" else "false"}" "'--with-cmdliner'" "${if
                                                                    cmdliner
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] ];
      preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
      [ ];
      installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
      createFindlibDestdir = true;
    }
