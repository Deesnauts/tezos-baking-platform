/*opam-version: "2.0"
  name: "magic-mime"
  version: "1.1.1"
  synopsis: "Map filenames to common MIME types"
  description: """
  This library contains a database of MIME types that maps filename
  extensions
  into MIME types suitable for use in many Internet protocols such as HTTP
  or
  e-mail.  It is generated from the `mime.types` file found in Unix systems,
  but
  has no dependency on a filesystem since it includes the contents of
  the
  database as an ML datastructure.
  
  For example, here's how to lookup MIME types in the [utop] REPL:
  
      #require "magic-mime";;
      Magic_mime.lookup "/foo/bar.txt";;
      - : bytes = "text/plain"
      Magic_mime.lookup "bar.css";;
      - : bytes = "text/css\""""
  maintainer: "Anil Madhavapeddy <anil@recoil.org>"
  authors: ["Anil Madhavapeddy" "Maxence Guesdon"]
  license: "ISC"
  homepage: "https://github.com/mirage/ocaml-magic-mime"
  doc: "https://mirage.github.io/ocaml-magic-mime/"
  bug-reports: "https://github.com/mirage/ocaml-magic-mime/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-magic-mime.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-magic-mime/releases/download/v1.1.1/magic-mime-v1.1.1.tbz"
    checksum: [
      "md5=8430a2686206517f2753e47c9c038b5c"
     
  "sha256=9a7f84ba37ca736fb7504ffe90e27fd58e18747c45a7b36b3aa77896fdf06639"
     
  "sha512=d1ec496dc72a879112b4fb71fecebaf543c2748bfc56079fc1411f165edeac378a4de966af108336a9002d291b489d951b185d9fce443836cccc2436e56f85ec"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.1"; in
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "magic-mime";
  version = "1.1.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-magic-mime/releases/download/v1.1.1/magic-mime-v1.1.1.tbz";
    sha256 = "0fb6y3yrcy5779mv79s5gis1i3nmgzi91zjga2vnywya6yx88zws";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
