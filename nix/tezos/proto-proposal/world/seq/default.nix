/*opam-version: "2.0"
  name: "seq"
  version: "base"
  synopsis:
    "Compatibility package for OCaml's standard iterator type starting from
  4.07."
  maintainer: " "
  authors: " "
  homepage: " "
  bug-reports: "https://caml.inria.fr/mantis/main_page.php"
  depends: [
    "ocaml" {>= "4.07.0"}
  ]
  dev-repo: "git+https://github.com/ocaml/ocaml.git"
  extra-files: [
    ["seq.install" "md5=026b31e1df290373198373d5aaa26e42"]
    ["META.seq" "md5=b33c8a1a6c7ed797816ce27df4855107"]
  ]*/
{ runCommand, doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv,
  opam, fetchurl, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "base"; in
assert (vcompare ocaml "4.07.0") >= 0;

stdenv.mkDerivation rec {
  pname = "seq";
  version = "base";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  postUnpack = "ln -sv ${./seq.install} \"$sourceRoot\"/seq.install\nln -sv ${./META.seq} \"$sourceRoot\"/META.seq";
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
