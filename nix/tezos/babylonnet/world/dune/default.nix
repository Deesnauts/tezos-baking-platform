/*opam-version: "2.0"
  name: "dune"
  version: "1.11.3"
  synopsis: "Fast, portable and opinionated build system"
  description: """
  dune is a build system that was designed to simplify the release of
  Jane Street packages. It reads metadata from "dune" files following a
  very simple s-expression syntax.
  
  dune is fast, it has very low-overhead and support parallel builds on
  all platforms. It has no system dependencies, all you need to build
  dune and packages using dune is OCaml. You don't need or make or bash
  as long as the packages themselves don't use bash explicitly.
  
  dune supports multi-package development by simply dropping
  multiple
  repositories into the same directory.
  
  It also supports multi-context builds, such as building against
  several opam roots/switches simultaneously. This helps maintaining
  packages across several versions of OCaml and gives cross-compilation
  for free."""
  maintainer: "Jane Street Group, LLC <opensource@janestreet.com>"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml/dune"
  doc: "https://dune.readthedocs.io/"
  bug-reports: "https://github.com/ocaml/dune/issues"
  depends: [
    "ocaml" {>= "4.02"}
    "base-unix"
    "base-threads"
  ]
  conflicts: [
    "jbuilder" {!= "transition"}
    "odoc" {< "1.3.0"}
    "dune-release" {< "1.3.0"}
  ]
  build: [
    ["ocaml" "configure.ml" "--libdir" lib] {opam-version < "2"}
    ["ocaml" "bootstrap.ml"]
    ["./boot.exe" "--release" "--subst"] {pinned}
    ["./boot.exe" "--release" "-j" jobs]
  ]
  dev-repo: "git+https://github.com/ocaml/dune.git"
  url {
    src:
     
  "https://github.com/ocaml/dune/releases/download/1.11.3/dune-build-info-1.11.3.tbz"
    checksum: [
     
  "sha256=c83a63e7e8245611b0e11d6adea07c6484dc1b4efffacb176315cd6674d4bbd2"
     
  "sha512=2c1532b91d223e6ea0628c5f5174792c1bb4113a464f6d8b878b3c58be1136beb84ba2d9883a330fa20e550367588aa923ba06ffb9b615a098a21374a9377e81"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base-unix, base-threads, findlib, fauxpam }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.11.3"; in
assert (vcompare ocaml "4.02") >= 0;

stdenv.mkDerivation rec {
  pname = "dune";
  version = "1.11.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/dune/releases/download/1.11.3/dune-build-info-1.11.3.tbz";
    sha256 = "1lmvsis6dk8mccbwpypz9qdxr134gjhdwshxw6q12mi4x3kn6fn8";
  };
  buildInputs = [
    ocaml base-unix base-threads findlib fauxpam ];
  propagatedBuildInputs = [
    ocaml base-unix base-threads fauxpam ];
  configurePhase = "true";
  patches = [
    ./dune-libdir.patch ];
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    (stdenv.lib.optionals ((vcompare "2.0.0" "2") < 0) [
      "'ocaml'" "'configure.ml'" "'--libdir'" "$OCAMLFIND_DESTDIR" ])
    [ "'ocaml'" "'bootstrap.ml'" ] [
      "'./boot.exe'" "'--release'" "'-j'" "1" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
