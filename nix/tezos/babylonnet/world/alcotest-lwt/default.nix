/*opam-version: "2.0"
  name: "alcotest-lwt"
  version: "0.8.5"
  synopsis: "Lwt-based helpers for Alcotest"
  maintainer: "thomas@gazagnaire.org"
  authors: "Thomas Gazagnaire"
  license: "ISC"
  homepage: "https://github.com/mirage/alcotest/"
  doc: "https://mirage.github.io/alcotest/"
  bug-reports: "https://github.com/mirage/alcotest/issues/"
  depends: [
    "dune"
    "ocaml" {>= "4.02.3"}
    "alcotest" {>= "0.8.0"}
    "lwt"
    "logs"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/alcotest.git"
  url {
    src:
     
  "https://github.com/mirage/alcotest/releases/download/0.8.5/alcotest-0.8.5.tbz"
    checksum: [
      "md5=2db36741c413ab93391ecc1f983aa804"
     
  "sha256=6b3b638fc7c6f4c52617b0261dc9f726ce21bb0c485e05bab6fe41a57697fc6b"
     
  "sha512=2d4aaec0a382fb4a883b3dc127e5a62272ecc721f6a1cc0bfe3aebba8bf76efc4bce2e097b2043a8b4b618323162f18d34b610ea8c12787c15210724a7523fa3"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, dune, ocaml, alcotest, lwt, logs, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.5"; in
assert (vcompare ocaml "4.02.3") >= 0;
assert (vcompare alcotest "0.8.0") >= 0;

stdenv.mkDerivation rec {
  pname = "alcotest-lwt";
  version = "0.8.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/alcotest/releases/download/0.8.5/alcotest-0.8.5.tbz";
    sha256 = "0szwjxvaahgynsx0apj81jxj3ki6yz4is9mh2wkcbx66qy7n6fvb";
  };
  buildInputs = [
    dune ocaml alcotest lwt logs findlib ];
  propagatedBuildInputs = [
    dune ocaml alcotest lwt logs ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
