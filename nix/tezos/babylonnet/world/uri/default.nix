/*opam-version: "2.0"
  name: "uri"
  version: "3.0.0"
  synopsis: "An RFC3986 URI/URL parsing library"
  description: """
  This is an OCaml implementation of the
  [RFC3986](http://tools.ietf.org/html/rfc3986) specification 
  for parsing URI or URLs."""
  maintainer: "anil@recoil.org"
  authors: ["Anil Madhavapeddy" "David Sheets" "Rudi Grinberg"]
  license: "ISC"
  tags: ["url" "uri" "org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-uri"
  doc: "https://mirage.github.io/ocaml-uri/"
  bug-reports: "https://github.com/mirage/ocaml-uri/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {>= "1.2.0"}
    "ounit" {with-test & >= "1.0.2"}
    "ppx_sexp_conv" {with-test & >= "v0.9.0"}
    "re" {>= "1.9.0"}
    "stringext" {>= "1.4.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-uri.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-uri/releases/download/v3.0.0/uri-v3.0.0.tbz"
    checksum: [
     
  "sha256=8fb334fba6ebbf879e2e82d80d6adee8bdaf6cec3bb3da248110d805477d19fa"
     
  "sha512=553c18032a7c96cccdc8e37f497ce34e821b9dd089cfc8685783b7ade1d4dfa422722e4724abcba8b1171b51fa91a2bee297396fc7c349118069b6352e07881e"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ounit ? null, ppx_sexp_conv ? null, re, stringext,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.0.0"; in
assert (vcompare ocaml "4.04.0") >= 0;
assert (vcompare dune "1.2.0") >= 0;
assert doCheck -> (vcompare ounit "1.0.2") >= 0;
assert doCheck -> (vcompare ppx_sexp_conv "v0.9.0") >= 0;
assert (vcompare re "1.9.0") >= 0;
assert (vcompare stringext "1.4.0") >= 0;

stdenv.mkDerivation rec {
  pname = "uri";
  version = "3.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-uri/releases/download/v3.0.0/uri-v3.0.0.tbz";
    sha256 = "1yhrgm3hbn0hh4jdmcrvxinazgg8vrm0vn425sg8ggzblvxk9cwg";
  };
  buildInputs = [
    ocaml dune ounit ppx_sexp_conv re stringext findlib ];
  propagatedBuildInputs = [
    ocaml dune ounit ppx_sexp_conv re stringext ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
