/*opam-version: "2.0"
  name: "macaddr"
  version: "4.0.0"
  synopsis: "A library for manipulation of MAC address
  representations"
  description: """
  A library for manipulation of MAC address representations.
  
  Features:
  
   * oUnit-based tests
   * MAC-48 (Ethernet) address support
   * `Macaddr` is a `Map.OrderedType`
   * All types have sexplib serializers/deserializers optionally via the
  `Macaddr_sexp` library."""
  maintainer: "anil@recoil.org"
  authors: ["David Sheets" "Anil Madhavapeddy" "Hugo Heuzard"]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-ipaddr"
  doc: "https://mirage.github.io/ocaml-ipaddr/"
  bug-reports: "https://github.com/mirage/ocaml-ipaddr/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {>= "1.9.0"}
    "ounit" {with-test}
    "ppx_sexp_conv" {with-test & >= "v0.9.0"}
  ]
  conflicts: [
    "ipaddr" {< "3.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-ipaddr.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-ipaddr/releases/download/v4.0.0/ipaddr-v4.0.0.tbz"
    checksum: [
     
  "sha256=6f4abf9c210b20ccddf4610691a87b8c870790d8f71d4a7edcfca9e21b59fc29"
     
  "sha512=ca55a8cfa8b84c0a2f4e1fe7afb4c582066bbb562efb94169c0347e441ce076dc426d191772edb869eca6bd77f42f7141378181057ad8886da25ef915a9ee57f"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ounit ? null, ppx_sexp_conv ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "4.0.0"; in
assert (vcompare ocaml "4.04.0") >= 0;
assert (vcompare dune "1.9.0") >= 0;
assert doCheck -> (vcompare ppx_sexp_conv "v0.9.0") >= 0;

stdenv.mkDerivation rec {
  pname = "macaddr";
  version = "4.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-ipaddr/releases/download/v4.0.0/ipaddr-v4.0.0.tbz";
    sha256 = "0agwb4dy5agwviz4l7gpv280g1wcgfl921k1ykfwq80b46fbyjkg";
  };
  buildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_sexp_conv findlib ];
  propagatedBuildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_sexp_conv ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
