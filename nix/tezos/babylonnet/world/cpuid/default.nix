/*opam-version: "2.0"
  name: "cpuid"
  version: "0.1.2"
  synopsis: "Detect CPU features"
  description: "CPUID"
  maintainer: "David Kaloper Meršinjak <dk505@cam.ac.uk>"
  authors: "David Kaloper Meršinjak <dk505@cam.ac.uk>"
  license: "ISC"
  homepage: "https://github.com/pqwy/cpuid"
  doc: "https://pqwy.github.io/cpuid/doc"
  bug-reports: "https://github.com/pqwy/cpuid/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {>= "1.7"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest"] {with-test}
  ]
  dev-repo: "git+https://github.com/pqwy/cpuid.git"
  url {
    src:
     
  "https://github.com/pqwy/cpuid/releases/download/v0.1.2/cpuid-v0.1.2.tbz"
    checksum: [
      "md5=21079a17bcf6cfe92e2f706b9d0d6d8d"
     
  "sha256=235572343128c7b71e9e5c2fc63161b3d9e32bc8abe77963597461a37625cf22"
     
  "sha512=c6bf37eccc7c9e61d8bbe288f1102fa02a704429e3ef6d4d3519b9a333f4b9001d5585914fbd2bcf1e6db0bd44595b7f3076c4b6f2476250d501a6e57f16bc2b"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.1.2"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.7") >= 0;

stdenv.mkDerivation rec {
  pname = "cpuid";
  version = "0.1.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/pqwy/cpuid/releases/download/v0.1.2/cpuid-v0.1.2.tbz";
    sha256 = "08ng4mva6qblb5ipkrxbr0my7ndkc4qwcbswkqgbgir864s74m93";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
