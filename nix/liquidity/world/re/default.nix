/*opam-version: "2.0"
  name: "re"
  version: "1.9.0"
  synopsis: "RE is a regular expression library for OCaml"
  description: """
  Pure OCaml regular expressions with:
  * Perl-style regular expressions (module Re.Perl)
  * Posix extended regular expressions (module Re.Posix)
  * Emacs-style regular expressions (module Re.Emacs)
  * Shell-style file globbing (module Re.Glob)
  * Compatibility layer for OCaml's built-in Str module (module
  Re.Str)"""
  maintainer: "rudi.grinberg@gmail.com"
  authors: [
    "Jerome Vouillon"
    "Thomas Gazagnaire"
    "Anil Madhavapeddy"
    "Rudi Grinberg"
    "Gabriel Radanne"
  ]
  license: "LGPL-2.0 with OCaml linking exception"
  homepage: "https://github.com/ocaml/ocaml-re"
  bug-reports: "https://github.com/ocaml/ocaml-re/issues"
  depends: [
    "ocaml" {>= "4.02"}
    "dune" {build}
    "ounit" {with-test}
    "seq"
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/ocaml/ocaml-re.git"
  url {
    src:
     
  "https://github.com/ocaml/ocaml-re/releases/download/1.9.0/re-1.9.0.tbz"
    checksum: "md5=bddaed4f386a22cace7850c9c7dac296"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  ounit ? null, seq, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.02") >= 0;

stdenv.mkDerivation rec {
  pname = "re";
  version = "1.9.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/ocaml-re/releases/download/1.9.0/re-1.9.0.tbz";
    sha256 = "1gas4ky49zgxph3870nffzkr6y41kkpqp4nj38pz1gh49zcf12aj";
  };
  buildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    seq findlib ];
  propagatedBuildInputs = [
    ocaml ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    seq ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
