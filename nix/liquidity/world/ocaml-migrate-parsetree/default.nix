/*opam-version: "2.0"
  name: "ocaml-migrate-parsetree"
  version: "1.2.0"
  synopsis: "Convert OCaml parsetrees between different versions"
  description: """
  Convert OCaml parsetrees between different versions
  
  This library converts parsetrees, outcometree and ast mappers
  between
  different OCaml versions.  High-level functions help making PPX
  rewriters independent of a compiler version."""
  maintainer: "frederic.bour@lakaban.net"
  authors: [
    "Frédéric Bour <frederic.bour@lakaban.net>"
    "Jérémie Dimino <jeremie@dimino.org>"
  ]
  license: "LGPL-2.1"
  tags: ["syntax" "org:ocamllabs"]
  homepage: "https://github.com/ocaml-ppx/ocaml-migrate-parsetree"
  doc: "https://ocaml-ppx.github.io/ocaml-migrate-parsetree/"
  bug-reports:
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/issues"
  depends: [
    "result"
    "ppx_derivers"
    "dune" {build & >= "1.6.0"}
    "ocaml" {>= "4.02.3" & < "4.08.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml-ppx/ocaml-migrate-parsetree.git"
  url {
    src:
     
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.2.0/ocaml-migrate-parsetree-v1.2.0.tbz"
    checksum: "md5=cc6fb09ad6f99156c7dba47711c62c6f"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml-result,
  ppx_derivers, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare dune "1.6.0") >= 0;
assert (vcompare ocaml "4.02.3") >= 0 && (vcompare ocaml "4.08.0") < 0;

stdenv.mkDerivation rec {
  pname = "ocaml-migrate-parsetree";
  version = "1.2.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.2.0/ocaml-migrate-parsetree-v1.2.0.tbz";
    sha256 = "143vykfspzymbq4qdk85g8gd004p12fxri5kwg1lj5war6739rjw";
  };
  buildInputs = [
    ocaml-result ppx_derivers dune ocaml findlib ];
  propagatedBuildInputs = [
    ocaml-result ppx_derivers dune ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
