/*opam-version: "2.0"
  name: "sodium"
  version: "0.6.0"
  synopsis: "Binding to libsodium UNAUDITED"
  description: """
  Binding to libsodium 1.0.9+, a shared library wrapper for djb's
  NaCl.
  
  Binding uses ctypes' stub generation system. GNU/Linux, FreeBSD, and OS
  X are supported.
  
  UNAUDITED"""
  maintainer: "sheets@alum.mit.edu"
  authors: [
    "David Sheets <sheets@alum.mit.edu>"
    "Peter Zotov <whitequark@whitequark.org>"
    "Benjamin Canou <benjamin@ocamlpro.com>"
  ]
  tags: "org:mirage"
  homepage: "https://github.com/dsheets/ocaml-sodium/"
  bug-reports: "https://github.com/dsheets/ocaml-sodium/issues/"
  depends: [
    "ocaml" {>= "4.01.0"}
    "base-bigarray"
    "base-bytes"
    "ocamlfind" {build}
    "conf-libsodium"
    "ctypes" {>= "0.6.0"}
    "ocamlbuild" {build}
  ]
  flags: light-uninstall
  build: [
    [make] {os != "freebsd"}
    [make "CFLAGS=-I/usr/local/include -L/usr/local/lib"] {os = "freebsd"}
    [make "test"] {with-test & os != "freebsd"}
    [make "CFLAGS=-I/usr/local/include -L/usr/local/lib" "test"]
      {with-test & os = "freebsd"}
  ]
  install: [make "PREFIX=%{prefix}%" "install"]
  remove: ["ocamlfind" "remove" "sodium"]
  post-messages: [
    "This package requires installation of libsodium-dev (>= 1.0.9)"
      {failure & os = "debian"}
    "This package requires installation of libsodium-dev (>= 1.0.9)"
      {failure & os = "ubuntu"}
    "This package requires installation of libsodium-dev (>= 1.0.9)"
      {failure & os = "alpine"}
    "This package requires installation of security/libsodium (>= 1.0.9)"
      {failure & os = "freebsd"}
    "This package requires installation of libsodium (>= 1.0.9)"
      {failure & os = "macos"}
  ]
  dev-repo: "git+https://github.com/dsheets/ocaml-sodium.git"
  url {
    src: "https://github.com/dsheets/ocaml-sodium/archive/0.6.0.tar.gz"
    checksum: "md5=c1e96c0f31fa12fad8b8b431eadc1601"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  base-bigarray, base-bytes, findlib, conf-libsodium, ctypes, ocamlbuild }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.01.0") >= 0;
assert (vcompare ctypes "0.6.0") >= 0;

stdenv.mkDerivation rec {
  pname = "sodium";
  version = "0.6.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/dsheets/ocaml-sodium/archive/0.6.0.tar.gz";
    sha256 = "0gilc2mg0kf4rag95cl507rajcasdpnff8idv8cf58c1b90lvqbf";
  };
  buildInputs = [
    ocaml base-bigarray base-bytes findlib conf-libsodium ctypes ocamlbuild ];
  propagatedBuildInputs = [
    ocaml base-bigarray base-bytes conf-libsodium ctypes ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] (stdenv.lib.optionals doCheck [ "make" "'test'" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'PREFIX='$out" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
