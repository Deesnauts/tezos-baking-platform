/*opam-version: "2.0"
  name: "ocamlbuild"
  version: "0.14.0"
  synopsis:
    "OCamlbuild is a build system with builtin rules to easily build most
  OCaml projects."
  maintainer: "Gabriel Scherer <gabriel.scherer@gmail.com>"
  authors: ["Nicolas Pouillard" "Berke Durak"]
  license: "LGPL-2 with OCaml linking exception"
  homepage: "https://github.com/ocaml/ocamlbuild/"
  doc:
  "https://github.com/ocaml/ocamlbuild/blob/master/manual/manual.adoc"
  bug-reports: "https://github.com/ocaml/ocamlbuild/issues"
  depends: [
    "ocaml" {>= "4.03"}
  ]
  conflicts: [
    "base-ocamlbuild"
    "ocamlfind" {< "1.6.2"}
  ]
  build: [
    [
      make
      "-f"
      "configure.make"
      "all"
      "OCAMLBUILD_PREFIX=%{prefix}%"
      "OCAMLBUILD_BINDIR=%{bin}%"
      "OCAMLBUILD_LIBDIR=%{lib}%"
      "OCAMLBUILD_MANDIR=%{man}%"
      "OCAML_NATIVE=%{ocaml:native}%"
      "OCAML_NATIVE_TOOLS=%{ocaml:native}%"
    ]
    [make "check-if-preinstalled" "all" "opam-install"]
  ]
  dev-repo: "git+https://github.com/ocaml/ocamlbuild.git"
  url {
    src: "https://github.com/ocaml/ocamlbuild/archive/0.14.0.tar.gz"
    checksum:
     
  "sha256=87b29ce96958096c0a1a8eeafeb6268077b2d11e1bf2b3de0f5ebc9cf8d42e78"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.03") >= 0;
assert !((vcompare findlib "1.6.2") < 0);

stdenv.mkDerivation rec {
  pname = "ocamlbuild";
  version = "0.14.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/ocamlbuild/archive/0.14.0.tar.gz";
    sha256 = "0y1fskw9rg2y1zgb7whv3v8v4xw04svgxslf3856q2aqd7lrrcl7";
  };
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "make" "'-f'" "'configure.make'" "'all'" "'OCAMLBUILD_PREFIX='$out"
      "'OCAMLBUILD_BINDIR='$out/bin" "'OCAMLBUILD_LIBDIR='$OCAMLFIND_DESTDIR"
      "'OCAMLBUILD_MANDIR='$out/man"
      "'OCAML_NATIVE='${if !stdenv.isMips then "true" else "false"}" "'OCAML_NATIVE_TOOLS='${if
                                                                    !stdenv.isMips
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] [ "make"
                                                                    "'check-if-preinstalled'"
                                                                    "'all'"
                                                                    "'opam-install'"
                                                                    ] ];
      preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
      [ ];
      installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
      createFindlibDestdir = true;
    }
