{ pkgs, lib, ... }:
let
  fetchTezosSrcPostFetch = rev: ''
    (
      set -Eeou pipefail
      echo "Tezos Baking Platform: fetchTezosSrcPostFetch: patching with git information"
      commit_hash=${rev}
      abbreviated_commit_hash=${builtins.substring 0 8 rev}

      git_info_ml="$out/src/lib_base/current_git_info.ml"
      if ! [ -f "$git_info_ml" ]; then
        git_info_ml="$out/src/lib_version/current_git_info.ml"
      fi
      function checked_replace() {
        sed -i "s/$1/$2/" "$git_info_ml"
        [ $(grep "$2" "$git_info_ml" | wc -l) == "1" ] || echo "Failed to replace $1 with $2 in $git_info_ml"
      }
      checked_replace "raw_commit_hash = .*." "raw_commit_hash = \"$commit_hash\""
      checked_replace "raw_abbreviated_commit_hash = .*." "raw_abbreviated_commit_hash = \"$abbreviated_commit_hash\""
    )
  '';
in rec {
  combineOverrides = old: new: (old // new) // {
    overrides = lib.composeExtensions old.overrides new.overrides;
  };
  makeRecursivelyOverridable = x: old: x.override old // {
    override = new: makeRecursivelyOverridable x (combineOverrides old new);
  };
  fetchThunk = p:
    if builtins.pathExists (p + /git.json)
      then pkgs.fetchgit { inherit (builtins.fromJSON (builtins.readFile (p + /git.json))) url rev sha256; }
    else if builtins.pathExists (p + /github.json)
      then pkgs.fetchFromGitHub { inherit (builtins.fromJSON (builtins.readFile (p + /github.json))) owner repo rev sha256; }
    else p;
  fetchThunkWithName = n: p:
    (fetchThunk p).overrideAttrs (drv: drv // { name = n; });
  fetchTezosSrc = p: sha256:
    if builtins.pathExists (p + /git.json)
      then let gitInfo = builtins.fromJSON (builtins.readFile (p + /git.json)); in pkgs.fetchgit {
        # NOTE: The hash from the obeliskch the hash of the transformed source after
        # `postFetch` so we must supply that separately. thunk won't mat
        inherit sha256;

        inherit (gitInfo) url rev;
        postFetch = fetchTezosSrcPostFetch gitInfo.rev;
        name = "tezos-src-${gitInfo.rev}-patched";
      }
    else p;
}
