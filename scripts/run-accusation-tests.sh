#!/usr/bin/env bash

set -Euo pipefail

fail() { "${___fail:?$1}"; }

root="$(git rev-parse --show-toplevel)"

cd "$root"

DO_BAKING='
  flextesa acc simple-double-baking --starting 35 --pause-to-display-kiln --with-kiln --kiln-dock "$DOCKER_IMAGE_ID" --pause-at-end true --tezos-node-binary "$(which tezos-node)" --tezos-client-binary "$(which tezos-client)"
'
DO_ENDORSEMENT='
  flextesa acc simple-double-endorsing --starting 35 --pause-to-display-kiln --with-kiln --kiln-dock "$DOCKER_IMAGE_ID" --pause-at-end true --tezos-node-binary "$(which tezos-node)" --tezos-client-binary "$(which tezos-client)"
'

case "x${1-}" in
  (x--endorsement-only) DO_BAKING=''; shift ;;
  (x--baking-only) DO_ENDORSEMENT=''; shift ;;
  (x--help | x-h | x-? | x--usage)
    echo "Usage: $0 [--endorsement-only | --baking-only | --help] [Kiln docker image]" 1>&2
    echo "" 1>&2
    echo "Test double baking and double endorsement scenarios.  If a docker image" 1>&2
    echo "identifier is not provided, one will be built based on the current thunk" 1>&2
    echo "for tezos-bake-monitor." 1>&2
    exit 0
    ;;
  (x-*) echo "Unknown option: $1" 1>&2; exit 1 ;;
esac

if [ "x${1-}" = "x" ]; then
  DOCKER_DRV='
    writeText "findDockerImage" (
      let bcd = (import ./. {}).bake-central-docker;
          dname = bcd.imageName;
          dtag0 = bcd.imageTag;
          dtag = if dtag0 == "" then builtins.head (builtins.split "-" (builtins.baseNameOf bcd.outPath)) else dtag0;
       in "export DOCKER_IMAGE_ID=${dname}:${dtag}; ${docker}/bin/docker inspect --type=image $DOCKER_IMAGE_ID >/dev/null 2>&1 || ${docker}/bin/docker load -i ${bcd}")
  '
else
  DOCKER_DRV='
    writeText "findDockerImage" "export DOCKER_IMAGE_ID='"'$1'"'"
  '
fi

nix-shell -p jq utillinux which docker nettools "$DOCKER_DRV" '(import ./. {}).tezos.flextesa.kit' --command "
  $DO_BAKING
  $DO_ENDORSEMENT
"
