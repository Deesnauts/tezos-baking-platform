# Using Liquidity Development Tools

Obsidian Systems' Baking Platform is able to build or install the toolchain for OCamlPro's Liquidity smart contract language.  See
[liquidity-lang.org](https://www.liquidity-lang.org/) for general information and [the documentation](https://www.liquidity-lang.org/doc/)
for tutorials and technical details.

To use Liquidity via the Baking Platform, start by running the following Nix command. It should take less time to run if you have [set up nix caching](https://gitlab.com/obsidian.systems/tezos-baking-platform#setting-up-nix-caching-recommended).

```
$ nix-build -A liquidity --no-out-link
```

Once that `nix-build` completes, you can enter a shell with the Liquidity compiler available at any time with the command

```
$ nix-shell -p '(import ./. {}).liquidity'
```

in the Baking Platform source directory, or from any directory by replacing `./.` by the path to the Baking Platform.

# One-off Commands

Without entering a shell, you can also use the Liquidity compilers `liquidity` and `liquidity.byte`, as you normally would by prefixing your commands with `$(nix-build -A liquidity --no-out-link)/bin/`.

For example, if you'd like to compile a contract called `simple.liq`, use the following command:

```
$ $(nix-build -A liquidity --no-out-link)/bin/liquidity simple.liq
```

To see all available binaries, run `ls $(nix-build -A liquidity --no-out-link)/bin/`.

# Adding Liquidity to Your Profile

You can also add Liquidity semi-permanently to your Nix profile with the command:

```
$ nix-env -i -f . -A liquidity
```

after which Liquidity should be available in your PATH at login.

# Updating the Liquidity Code

If you wish to use newer or forked versions of the upstream Tezos Core code, a script is provided to assist with this.  First make sure that the thunk in `liquidity` points to a git repository and branch containing the code you wish to use; see the [obelisk](https://github.com/obsidiansystems/obelisk/) documentation for details.  Then run

```
$ scripts/bump-liquidity.sh
```

The script may require you to answer some prompts.

This should do everything required; however, make sure to run a test build of Liquidity afterward.
