#!/usr/bin/env bash
set -Eeuo pipefail

here="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# For debugging:
# export TEZOS_LOG="client.signer.ledger -> debug"

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y
source "$here/sandbox-init.sh"

attempt() {
    echo "$@"
    while ! "$@"; do
        echo 'Failed'
        sleep 3
    done
    sleep 2
}
extract_operation_id() {
    tail -n1 | cut -f2 -d"'"
}

burn_cap=0.257
transfer() {
    tezos-sandbox-client.sh transfer "$@" --burn-cap $burn_cap
}

attempt transfer 2000000 from bootstrap0 to my-ledger
attempt transfer 2000001 from bootstrap1 to my-ledger

echo "Please start out with ledger in baking app"
sleep 5
attempt tezos-sandbox-client.sh set delegate for my-ledger to my-ledger

echo "Confirm that baking app rejects each of the following attempts, then switch to wallet app"
sleep 5
attempt transfer 12 from my-ledger to bootstrap1
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
sleep 5
attempt tezos-sandbox-client.sh originate account original for my-ledger transferring 40 from my-ledger --delegatable --burn-cap $burn_cap
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
sleep 5
attempt transfer 15 from original to bootstrap1
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
sleep 5
attempt transfer 13 from my-ledger to original
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
sleep 5
tezos-sandbox-client.sh set delegate for original to my-ledger || :
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
sleep 5
tezos-sandbox-client.sh withdraw delegate from my-ledger || :
echo "Switch back to baking app to confirm that this will reject, then use wallet app to sign"
tezos-sandbox-client.sh set delegate for my-ledger to my-ledger --fee 1 || :

echo "Switch back to baking app"
sleep 5
attempt tezos-sandbox-client.sh set ledger high watermark for "$ledger_id" to 10000
attempt tezos-sandbox-client.sh get ledger high watermark for "$ledger_id"
attempt tezos-sandbox-client.sh set ledger high watermark for "$ledger_id" to 0
attempt tezos-sandbox-client.sh get ledger high watermark for "$ledger_id"

tezos-sandbox-baker.sh run with local node sandbox/node-1 my-ledger & pid=$!
tezos-sandbox-endorser.sh run my-ledger & pid2=$!
wait $pid $pid2
