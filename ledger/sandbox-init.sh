#!/usr/bin/env bash
set -Eeuxo pipefail

here="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

ledger_id=$([[ $(tezos-client list connected ledgers) =~ \"(ledger://[^\"]*)\" ]] && echo "${BASH_REMATCH[1]}")
if [ "$ledger_id" = "" ]; then
  >&2 echo 'Unable to find a connected ledger. Is the ledger connected and open to the Wallet or Baking app?'
  exit 1
fi
echo "Using ledger $ledger_id"

"$here"/../scripts/sandbox-nodes.sh
bootstrap-baking.sh
tezos-sandbox-client.sh import secret key my-ledger "$ledger_id"
tezos-sandbox-client.sh transfer 1000000 from bootstrap0 to my-ledger --burn-cap 0.257
