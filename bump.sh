#!/usr/bin/env bash

set -eu

PWD="$(pwd)"

export base_nixshell="nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync cacert"

if [ ! -d "bump/opam-root" ]; then
  mkdir -p bump
  $base_nixshell 'callPackage nix/opam_2_0_0.nix {}' --run 'opam init --root=bump/opam-root -n --bare'
fi

export OPAMROOT="$PWD"/bump/opam-root

if [ ! -d "bump/opam-nixify" ]; then
  mkdir -p bump
  git clone --single-branch --branch eac@fix https://github.com/obsidiansystems/opam-nixify.git bump/opam-nixify
fi

if [ ! -d "bump/opam-nixify/_opam" ]; then
  mkdir -p bump
  $base_nixshell 'callPackage nix/opam_2_0_0.nix {}' --run 'opam switch --root=bump/opam-root create bump/opam-nixify ocaml-base-compiler.4.06.1'
fi

$base_nixshell 'callPackage nix/opam_2_0_0.nix {}' --run 'export OPAMROOT="$(pwd)/bump/opam-root"; cd bump/opam-nixify && eval $(opam env --switch=. --set-switch) && opam install --yes -w opam-nixify'

for NETWORK in "$@"; do
  ob thunk update tezos/"$NETWORK"
  mkdir -p bump/tezos
  rm -rf bump/tezos/"$NETWORK"
  cp -r tezos/"$NETWORK" bump/tezos/
  ob thunk unpack bump/tezos/"$NETWORK"
  $base_nixshell which autoconf pkgconfig libev gmp hidapi 'callPackage nix/tezos/'"$NETWORK"'/opam.nix {}' --run 'cd bump/tezos/'"$NETWORK"'; export OPAMROOT="$(pwd)/.opam"; opam init -n --bare; make build-deps'
  rm -rf nix/tezos/"$NETWORK"/world
  export OPAMROOT="$PWD/bump/tezos/$NETWORK/.opam"
  OPAMVAR_network="$NETWORK" bump/opam-nixify/_opam/bin/opam-nixify --switch "$PWD/bump/tezos/$NETWORK" --settings=tezos.nixpam
  TEZOS_NEEDED=$($base_nixshell 'callPackage nix/opam_2_0_0.nix {}' --run '
    export LC_ALL=C
    cd bump/tezos/'"$NETWORK"'
    opam show --root=.opam --switch=. -f depends: $(find "vendors" "src" -name \*.opam -print) \
      | perl -e '\''
         while ($x=<>) {
           $x =~ s/"([^"]*)"(?:\s+[{](.*?)[}])?\s*/
                   (grep { $_ eq "with-test" } split \/\s*&\s*\/, $2) ? "" : "$1\n"/eg;
           print $x;
         }'\'' \
      | sort | uniq \
      | join -v1 - <(find "vendors" "src" -name \*.opam -print | xargs -d'\''\n'\'' -n1 -- basename -s.opam | sort) \
      | perl -pe '\''s/^hidapi\n/ocaml-hidapi\n/; s/^ocamlfind\n/findlib\n/; '\''
    '
  )
  DEPENDS_COMMAS=""
  DEPENDS_SPACES=""
  for i in $TEZOS_NEEDED; do
    DEPENDS_COMMAS="$DEPENDS_COMMAS, $i"
    DEPENDS_SPACES="$DEPENDS_SPACES $i"
  done
  export DEPENDS_COMMAS
  export DEPENDS_SPACES
  perl -pe 's/, __DEPENDS_COMMAS__/$ENV{DEPENDS_COMMAS}/g; s/ __DEPENDS_SPACES__/$ENV{DEPENDS_SPACES}/g' nix/tezos/core.nix.template >nix/tezos/"$NETWORK"/core.nix
done
