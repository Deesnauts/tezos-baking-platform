To allow users to build without setting up github/gitlab accounts, submodules use 'https' by default. You can use 'ssh' authentication by adding the following options in your `.gitconfig` file.

```config
[url "git@gitlab.com:"]
     insteadOf = https://gitlab.com/

[url "git@github.com:"]
     insteadOf = https://github.com/
```
