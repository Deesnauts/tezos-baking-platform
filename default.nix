{ nixpkgs ? import nix/nixpkgs.nix {}
}:
nixpkgs.lib.makeScope nixpkgs.newScope (self:
let
  inherit (self) callPackage;
  pkgs = nixpkgs;
  inherit (nixpkgs) fetchgit haskell;
  tezos-bake-monitor-src = import nix/pins/tezos-bake-monitor;
in
rec {
  pkgs = self;
  lib = nixpkgs.lib.extend (self: super: {
    versioning =
      if super ? versioning then super.versioning
      else import ./nix/lib/versioning { lib = self; };
  });
  stdenv = nixpkgs.gccStdenv // {
    lib = nixpkgs.gccStdenv.lib.extend (self: super: {
      versioning =
        if super ? versioning then super.versioning
        else lib.versioning;
    });
  };

  utils = nixpkgs.callPackage nix/lib/utils.nix {};

  fauxpam = callPackage nix/fauxpam.nix {};

  tezos = rec {
    mkWithSrc = { net, src, sha256 }: callPackage nix/tezos {
      tezos-src = utils.fetchTezosSrc src sha256;
      tezos-world-path = nix/tezos + net + /world;
      tezos-core-path = nix/tezos + net + /core.nix;
    };
    mk = { net, sha256 }: mkWithSrc { inherit net sha256; src = (tezos/. + net); };
    master = mk { net = /master; sha256 = "0js8m4vpqxrw7r83wjpcbczy66q0acc7c8sys9yrdbirkmv9h4zy"; };
    zeronet = mk { net = /zeronet; sha256 = "0d5p26rjlx3b3bxfvxw63k8kphvb1ana8fqr15adw47g9z742zs1"; };
    alphanet = mk { net = /alphanet; sha256 = "1s3dpif5ndhbvmmgsps4mmi82j9niy41hl595xgby20n0nx7a8nn"; };
    mainnet = mk { net = /mainnet; sha256 = "0jscybfyjdpsg73lcqq09zg6mkmhhmdj66szzajh8rbimli602d8"; };
    proto-proposal = mk { net = /proto-proposal; sha256 = "10437ayy4dw0vg572vyvkllmwiil2wqdzkqcgd07wwmxd7a37vns"; };
    flextesa-dev = mkWithSrc { net = /master; src = tezos/flextesa-dev; sha256 = "08lvxh40ac7113xrac2mg7w18mmzk89xc93lihrl22ynk2087p8g"; };
    babylonnet = mk { net = /babylonnet; sha256 = "0kz3zlcz6h11bmhnmcmbw4xxds6yxvgbhsd1g8mwna0qb79rvj6p"; };
    carthagenet = mk { net = /carthagenet; sha256 = "1353a9x72wg2yfvhbxf6xh1yi7ax0mzyyxdj4r847cf75vb6zwbh"; };
  };

  inherit (nixpkgs) ocaml-ng;
  liquidity =
    let
      world = (callPackage nix/liquidity/world {}).extend (self: super: {
        ocp-build = super.ocp-build.overrideAttrs (old: old // {
          setupHook = nixpkgs.writeText "setupHook.sh" ''
            addOcp2Path () {
                if test -d "''$1/lib/ocaml/site-ocp2"; then
                    export OCPBUILD_OCP_DIRS="''${OCPBUILD_OCP_DIRS}''${OCPBUILD_OCP_DIRS:+:}''$1/lib/ocaml/site-ocp2/"
                fi
            }

            addEnvHooks "$hostOffset" addOcp2Path

            destDirInOcamlPath () {
                export OCAMLPATH="''${OCAMLFIND_DESTDIR}''${OCAMLPATH:+:}''${OCAMLPATH}"
            }

            postPatchHooks+=("destDirInOcamlPath")
          '';
        });
      });
    in
    world.callPackage nix/liquidity { inherit (utils) fetchThunkWithName; };

  obelisk = import ((utils.fetchThunk ./tezos-bake-monitor) + /tezos-bake-central/.obelisk/impl) {};
  reflex-platform = obelisk.reflex-platform;
  obeliskNixpkgs = reflex-platform.nixpkgs;
  tezos-loadtest = obeliskNixpkgs.haskellPackages.callCabal2nix "tezos-loadtest" (reflex-platform.hackGet ./tezos-load-testing) {};

  tezos-bake-central = (import ./tezos-bake-monitor {}).exe;
  bake-central-docker = (import ./tezos-bake-monitor {}).dockerImage;

  ledger = nixpkgs.callPackage ledger/ledger-app { commit = "TEST"; };
})
